library(shiny)

# Define UI
ui <- fluidPage(
  
  # Add a title
  titlePanel("VPN GUI"),
  
  # Add a sidebar with input controls
  sidebarLayout(
    sidebarPanel(
      sliderInput("num_connections", "Number of VPN connections:", min = 1, max = 10, value = 1),
      textInput("server_name", "VPN server name:"),
      numericInput("server_capacity", "VPN server capacity:", min = 1, max = 1000, value = 1)
    ),
    
    # Add a main panel for displaying output
    mainPanel(
      h3("VPN status:"),
      textOutput("status")
    )
  )
)

# Define server logic
server <- function(input, output) {
  
  # Calculate and display the VPN status based on the number of connections and server capacity
  output$status <- renderText({
    if (input$num_connections > input$server_capacity) {
      "High number of VPN connections for the specified server"
    } else {
      paste("VPN server", input$server_name, "has sufficient capacity for the number of connections")
    }
  })
}

# Run the Shiny app
shinyApp(ui = ui, server = server)
